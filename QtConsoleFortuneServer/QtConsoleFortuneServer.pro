QT += core network

TARGET = QtConsoleFortuneServer
CONFIG   += console
CONFIG   -= app_bundle

HEADERS       = server.h
SOURCES       = server.cpp \
                main.cpp

DESTDIR = bin
				
# install
INSTALLS += target
