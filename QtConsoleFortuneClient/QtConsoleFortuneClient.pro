QT += core network
QT -= gui

TARGET = QtConsoleFortuneClient
CONFIG   += console
CONFIG   -= app_bundle

HEADERS       = client.h
SOURCES       = client.cpp \
                main.cpp

DESTDIR = bin

# install
INSTALLS += target
